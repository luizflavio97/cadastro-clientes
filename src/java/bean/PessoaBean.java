
package bean;

import entity.Pessoa;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class PessoaBean {
    
    private Pessoa pessoa = new Pessoa();
    ArrayList<Pessoa> listaPessoas = new ArrayList<>();
    private String auxCpf;
    private boolean atualiza = false;

    public void addPessoa(){
        if (pessoa.getNome().isEmpty() || pessoa.getSobrenome().isEmpty()
                || pessoa.getCpf().isEmpty() || pessoa.getRg() == null){
            System.out.println("Campos a serem preenchidos");
        } else {
            listaPessoas.add(pessoa);
            pessoa = new Pessoa();
        }
    }
    
    public void getPessoaPorCPF(){
        String cpf = getAuxCpf();
        
        for (int i = 0; i < listaPessoas.size(); i++) {
            pessoa = listaPessoas.get(i);
            
            if(pessoa.getCpf().equals(cpf)){
                setAtualiza(true);
            } else {
                pessoa = new Pessoa();
            }
        }
        
    }
    
    public void atualizaPessoa(){
        String cpf = getAuxCpf();
        
        for (int i = 0; i < listaPessoas.size(); i++){
            if ( listaPessoas.get(i).getCpf().equals(cpf)) {
                listaPessoas.get(i).setNome(pessoa.getNome());
                listaPessoas.get(i).setSobrenome(pessoa.getSobrenome());
                listaPessoas.get(i).setCpf(pessoa.getCpf());
                listaPessoas.get(i).setRg(pessoa.getRg());
                
                setAuxCpf("");
                setAtualiza(false);
                pessoa = new Pessoa();
            }
        }
    }
    
    public void removePessoa(){
        String cpf = getAuxCpf();
        
        for (int i = 0; i < listaPessoas.size(); i++) {
            if ( listaPessoas.get(i).getCpf().equals(cpf)) {
                listaPessoas.remove(i);
                
                setAuxCpf("");
                pessoa = new Pessoa();
            }
        }
    }
    
    public void limpar(){
        pessoa = new Pessoa();
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public ArrayList<Pessoa> getListaPessoas() {
        return listaPessoas;
    }

    public void setListaPessoas(ArrayList<Pessoa> listaPessoas) {
        this.listaPessoas = listaPessoas;
    }

    public String getAuxCpf() {
        return auxCpf;
    }

    public void setAuxCpf(String auxCpf) {
        this.auxCpf = auxCpf;
    }

    public boolean isAtualiza() {
        return atualiza;
    }

    public void setAtualiza(boolean atualiza) {
        this.atualiza = atualiza;
    }
    
    
     
}
